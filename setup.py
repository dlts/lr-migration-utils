from setuptools import setup

setup(name='Liferay Migraion Utils',
    version='0.1',
    description='Provides python wrappers for liferay json webservices as well as custom interactions for migration purposes',
    #url='http://github.com/storborg/funniest',
    author='Darnell Lynch',
    author_email='darnell.lynch@marist.edu',
    license='MIT',
    packages=['lr_migration_util'],
    install_requires=[
        'requests',
        'bs4',
        'HTMLParser',
        'MySQL-python'

    ],
    zip_safe=False)
