# -*- coding: utf-8 -*-

from .liferay_api import LiferayAPI
from . import utils


class LRM(LiferayAPI):

    def __init__(self,host,user,password,group_id,company_id,layouts=[]):
        super(LRM, self).__init__(host,user,password,group_id,company_id,layouts)
        self._is_connected = self._ping()

    def is_connected(self):
        return self._is_connected

    def set_dl_permission_to_guest(self, dl_obj):
        print(dl_obj)
        pass

    def migrate_url_images(self, url, pre_fetched_html='', target_selector = ''):
        # set target selector
        selector = ('img' if not target_selector else target_selector)

        data = ''
        # prevent call if going to use pre_fetched, though not validating
        if pre_fetched_html:
            data = pre_fetched_html
        else:
            # can repair or handle in parent better
            self.REQ.OBJType = ''
            if self.REQ.getREQ(url):
                data = self.REQ.data
            self.REQ.OBJType = 'LR'

        if not data:
            print('no data')
            return

        img_data = []
        images = utils.get_images_from_html(data, url, selector)
        if not images:
            print('no images')
            return
        else:
            for image_url in images:
                # create image in doc lib
                if self._add_dl_file(image_url[0]):
                    image_basename = image_url[0].split("?")[0].split("/")[-1]

                    if self.get_document_library_file(self.DL_FOLDER, image_basename):
                        document_obj = self.REQ.data
                        document_url = self.build_dl_url(document_obj)

                        # todo: will need to break out expando as an object that is passed in abstract manner
                        # assumes prev_href is set up !yikes
                        if self.get_dl_latest_file_version(document_obj['fileEntryId']):
                            if self.add_expando_value(
                                "com.liferay.portlet.documentlibrary.model.DLFileEntry",
                                "CUSTOM_FIELDS",
                                "prev_href",
                                self.REQ.data['fileVersionId'],
                                image_url[1],
                            ):
                                print('IMage added:{}'.format(image_basename))
                                img_data.append((image_url[1], document_url))
        return img_data

    def migrate_page_content(self, url, target_layout=None):
        url = (url[:-1] if '/' == url[-1] else url)

        # can repair or handle in parent better
        self.REQ.OBJType = ''
        html = (self.REQ.data if self.REQ.getREQ(url) else '')
        self.REQ.OBJType = 'LR'
        if not html:
            print('no data')
            return

        content = utils.get_content_from_html(html)
        migrate_images = self.migrate_url_images(url, pre_fetched_html=content)

        if migrate_images:
            content = content.decode('utf-8')
            for image in migrate_images:
                content = content.replace(image[0],image[1])
                print('replacing {} with {}'.format(image[0], image[1]))

        if not self.get_layouts():
            print('didnt get layouts')
            exit(1)

        urlparsed = utils.urlparse(url)
        friendly = urlparsed.path
        layout_id = None
        layout_plid = None

        if friendly not in self.friendlyURLs:
            folder_id = raw_input(
                "This page doesn't exist yet, in order to create it, please provide a parent folder...")
            page_name = raw_input("Name of page?")
            # CREATE LAYOUT
            if not self.create_layout(page_name, page_name, friendly, folder_id):
                print('layout didnt create')
                exit(1)
            layout_id = self.REQ.data['layoutId']
            layout_plid = self.REQ.data['plid']
        else:
            layout_id = self.friendlyURLs[friendly]['id']
            page_name = self.friendlyURLs[friendly]['name']
            layout_plid = self.friendlyURLs[friendly]['plid']

        if not layout_id:
            print('check your code')
            exit(1)

        # CREATE CONTENT
        if not self.get_articles():
            print('couldnt get articles from LR')
            exit(1)

        articles = self.REQ.data
        articles_by_title = [a['titleCurrentValue'] for a in articles]
        article = {}

        if page_name in articles_by_title:
            article = articles[articles_by_title.index(page_name)]
            print('yes it is')
            if self.update_article({
                'folderId': 0,
                'articleId': article['articleId'],
                'version': article['version'],
                'content': self.genXMLContent(content, True)
            }):
                print('update successful')
        else:
            print('its not')
            if self.add_article({
                'folderId': 0,
                'titleMap': page_name,
                'descriptionMap': page_name,
                'content': self.genXMLContent(content, True)
            }):
                article = self.REQ.data
                print('good to go')
            else:
                print('something wrong')
                exit(1)

        # are there other ways to populate layouts?
        layouts = self.LAYOUT_TEMPLATES
        layout_prop = -1

        if target_layout:
            try:
                target_layout = int(target_layout)

            except ValueError as e:
                print(e)
                exit(1)
            else:
                try:
                    layouts[target_layout]
                except IndexError as e:
                    print e
                    exit(1)
                layout_prop = target_layout
        else:

            while True:
                layout_prop = int(raw_input(
                    'Choose a layout: \n{}\n'.format('\n'.join(["{} : {}".format(k, i) for k, i in enumerate(layouts)]))))

                if layout_prop >= len(layouts):
                    print('must choose a valid layout, Please try again')
                    continue
                else:
                    break

        desired_layout = layouts[layout_prop]
        hash_val = self.TypeSettings.gen_type_setting_hash()

        column = '2'
        if layout_prop in [2,3]:
            column = '1'

        ts = self.TypeSettings(type_setting=self.friendlyURLs[friendly]['typeSettings'])
        # ts.columns[column]['hashes'] = [hash_val]
        ts.append_hash_to_type_settings_column(hash_val,column,True)
        ts.layout_template_id = desired_layout
        ts.render_type_setting()
        #
        # [
        #     'column-1=',
        #     'column-{}={},'.format(2, hash_val),
        #     'column-{}-customizable=false'.format(2),
        #     'layout-template-id={}'.format(desired_layout),
        #     'layoutUpdateable=true',
        #     'sitemap-changefreq=daily',
        #     'sitemap-include=1'
        # ]

        if not self.add_article_to_layout(
            layout_id=layout_id,
            layout_plid=layout_plid,
            article_id=article['articleId'],
            type_setting=ts.TS,
            hash_val=hash_val,
            purge_articles=True,
            articles_not_to_purge=[] if '1' not in ts.columns else ts.columns['1']['hashes']
        ):
            print('something went wrong adding article to layout')
            exit(1)

        return utils.log_pages(1,url, self.RAW_HOST, desired_layout)


    def add_portlet_to_layout(self, portlet, column):
        # add porlet
        # build typesetting

        print('here')
        pass