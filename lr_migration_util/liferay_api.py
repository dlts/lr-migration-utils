# -*- coding: utf-8 -*-

import urllib2
from HTMLParser import HTMLParser
from xml.etree import cElementTree as ET
import random

import urllib2
import base64

from .req import REQ
from .liferay_structures import *


class LiferayAPI(object):

    def __init__(self,host,user,password,group_id,company_id,layouts):
        self.RAW_HOST = host
        self.HOST = "%s%s" % (host, '/api/jsonws')
        self.USER = user
        self.PASSWORD = password
        self.GROUP_ID = group_id
        self.COMPANY_ID = company_id
        self._LAYOUT_TEMPLATES = []
        if layouts:
            self._set_layouts(layouts)

        self._MYSQL_CONN = {}

        self.REQ = REQ({
            'host': self.HOST,
            'auth': (
                self.USER, self.PASSWORD
            )
        })
        self.REQ.OBJType = 'LR'
        self.DB_COUNT = 0
        self.DL_FOLDER = 0

        self.LAYOUT = {}
        self.LAYOUTS = []
        self.LAYOUT_REQ_DB = {}
        self.LAYOUT_IDS = []
        self.friendlyURLs = {}

        self.ARTICLE_FOLDER = 0

        self.TS = []

    def _ping(self):
        self.REQ.endpoint = '/group/get-group'
        self.REQ.data = [
            ('groupId', self.GROUP_ID)
        ]
        return self.REQ.getREQ()

    # LAYOUT STUFF

    def _set_layouts(self, layouts):
        self.LAYOUT_TEMPLATES = layouts

    def get_layouts(self, private=False):
        self.REQ.endpoint = '/layout/get-layouts'
        self.REQ.data = [
            ('groupId', self.GROUP_ID),
            ('privateLayout', private),
        ]
        if self.REQ.postREQ():
            self.LAYOUTS = self._parse_layouts(self.REQ.data)
            for layout in self.LAYOUTS:
                self.friendlyURLs[layout['friendlyURL']] = {
                    'id': layout['layoutId'],
                    'parent_id': layout['parentLayoutId'],
                    'name': layout['name'],
                    'typeSettings': layout['typeSettings'],
                    'plid': layout['plid']
                }

        return True if self.LAYOUTS else False

    def _parse_layout(self, LAYOUT):
        targetted_keys = ['layoutId', 'parentLayoutId', 'friendlyURL', 'name', 'typeSettings', 'plid']
        new_layout_obj = {}
        for k, v in LAYOUT.iteritems():
            if k in targetted_keys:
                if k == 'name': v = self.retXMLValue(v, k.capitalize())
                new_layout_obj[k] = v
            if k == 'layoutId':
                self.LAYOUT_IDS.append(v)
        return new_layout_obj

    def _parse_layouts(self, layouts):
        parsed_layouts = [self._parse_layout(l) for l in layouts]

        for k, item in enumerate(parsed_layouts):
            self.LAYOUT_REQ_DB[item['layoutId']] = layouts[k]

        return parsed_layouts

    def create_layout(self, name, title, friendly_url, parent_id=0, renderTypeSetting=False):
        self.REQ.endpoint = '/layout/add-layout'
        data = [
            ('groupId', self.GROUP_ID),
            ('privateLayout', 'false'),
            ('type', 'portlet'),
            ('hidden', 'false'),

            ('parentLayoutId', parent_id),
            ('name', name),
            ('title', title),
            ('description', ''),
            ('friendlyURL', friendly_url),
        ]
        self.REQ.data = data
        result = self.REQ.postREQ()
        if result:
            self.LAYOUT = self.REQ.data
            if renderTypeSetting:
                if not self.update_layout3(self.REQ.data['layoutId']):
                    print("Failed to add type setting")

            self.LAYOUTS.append(self.LAYOUT)
            self.friendlyURLs[self.LAYOUT['friendlyURL']] = {
                'id': self.LAYOUT['layoutId'],
                'parent_id': self.LAYOUT['parentLayoutId'],
                'name': self.LAYOUT['name'],
                'typeSettings': self.LAYOUT['typeSettings']
            }

        return result

    def update_layout1(self, layout):
        self.REQ.endpoint = '/layout/update-layout'
        self.REQ.renderData(layout)
        result = self.REQ.getREQ()
        self.LAYOUT = self.REQ.data
        return result

    def update_layout3(self, layout_id, type_setting=''):
        # type_setting = (self._renderTypeSetting() if not type_setting else type_setting)
        if not type_setting: return False
        self.REQ.endpoint = '/layout/update-layout'
        self.REQ.data = [
            ('groupId', self.GROUP_ID),
            ('privateLayout', 'false'),
            ('typeSettings', type_setting),
            ('layoutId', layout_id)
        ]
        result = self.REQ.postREQ()
        self.LAYOUT = self.REQ.data
        return result

    # END LAYOUT

    # JOURNAL ARTICLE STUFF
    def add_article(self, data):
        self.REQ.endpoint = '/journalarticle/add-article'
        article = Article(data, groupId=self.GROUP_ID)
        article.folderId = self.ARTICLE_FOLDER
        self.REQ.renderData(article)
        return self.REQ.postREQ()

    def get_articles(self, folder_id=0):
        self.REQ.endpoint = '/journalarticle/get-articles'
        self.REQ.data = [
            ('groupId', self.GROUP_ID),
            ('folderId', folder_id)
        ]
        return self.REQ.postREQ()

    def get_articles_by_folder(self, folderId=0):
        self.REQ.endpoint = '/journalarticle/get-articles'
        self.REQ.data = [
            ('groupId', self.GROUP_ID),
            ('folderId', folderId)
        ]
        return self.REQ.postREQ()

    def update_article(self, data):
        self.REQ.endpoint = '/journalarticle/update-article'
        data['groupId'] = self.GROUP_ID
        self.REQ.data = []
        for k, v in data.iteritems():
            self.REQ.data.append((k, v))
        return self.REQ.postREQ()

    # END JOURNAL ARTICLES

    # DOCUMENT LIBRARY STUFF

    def _add_dl_file(self, url=''):

        self.REQ.endpoint = '/dlapp/add-file-entry'
        print url
        f = urllib2.urlopen(url)
        basename = url.split("?")[0].split("/")[-1]

        D = {
            'folderId': self.DL_FOLDER,
            'sourceFileName': '{}.jpg'.format(basename),
            'mimeType': 'image/jpeg',
            'title': basename,
            'description': '',
            'file': f.read()
        }

        dl_file = DLFile(D, self.GROUP_ID)
        self.REQ.renderData(dl_file)
        self.REQ.data.append(('serviceContext.addGuestPermissions', True))
        if self.REQ.postREQ(multipart=True):
            return True
        if self.REQ.exception and self.REQ.exception[
            'code'] == 'com.liferay.portlet.documentlibrary.DuplicateFileException':
            if self.get_document_library_file(self.DL_FOLDER, basename):
                print('IMAGE DUP!!! STILL True')
                return True
        return False

    def get_document_library_file_entries(self, repository_id=0, folder_id = 0):
        if not repository_id:
            repository_id = self.GROUP_ID
        self.REQ.endpoint = '/dlapp/get-file-entries'
        self.REQ.data = [
            ('repositoryId', repository_id),
            ('folderId', folder_id)
        ]
        return self.REQ.postREQ()

    def get_document_library_file(self, folder_id, title):
        self.REQ.endpoint = '/dlapp/get-file-entry'
        self.REQ.data = [
            ('groupId', self.GROUP_ID),
            ('folderId', folder_id),
            ('title', title)
        ]
        return self.REQ.postREQ()

    def buildDL_URL_By_DL_OBJ(self, domain, d):
        return "{}{}".format(domain, self.buildDL_URL(d['repositoryId'], d['folderId'], d['title'].replace(" ", "+"),
                                                      d['uuid']))

    def buildDL_URL(self, repoId, folderId, name, uuid):
        return "/documents/{}/{}/{}/{}".format(repoId, folderId, name, uuid)

    def update_document_library_file(self, d):

        print(self.buildDL_URL_By_DL_OBJ(self.RAW_HOST, d))
        request = urllib2.Request(self.buildDL_URL_By_DL_OBJ(self.RAW_HOST, d))
        base64string = base64.encodestring('%s:%s' % (self.USER, self.PASSWORD)).replace('\n', '')
        request.add_header("Authorization", "Basic %s" % base64string)
        f = urllib2.urlopen(request)
        print(f.read())
        exit()

        # mim = d['mimeType'].split('/')[1]
        # source_file_name = '{}.{}'.format(d['title'], extension)
        # if d['mimeType'] == 'image/jpeg'

        D = {
            'folderId': d['folderId'],
            'sourceFileName': d['title'],
            'mimeType': d['mimeType'],
            'title': d['title'],
            'description': d['description'],
            'file': f.read()
            # 'file' : get_img.text.read()
        }
        self.REQ.endpoint = '/dlapp/update-file-entry'

        dl_file = DLFile(D, self.GROUP_ID)
        dl_file.changeLog = d['version']

        self.REQ.renderData(dl_file)
        # self.REQ.data.append(('serviceContext.addGroupPermissions',True))
        self.REQ.data.append(('serviceContext.addGuestPermissions', 1))
        self.REQ.data.append(('majorVersion', False))
        self.REQ.data.append(('fileEntryId', d['fileEntryId']))

        # for i in self.REQ.data:
        #     if i != 'file':
        #         print i

        return self.REQ.postREQ(multipart=True)

    def get_dl_latest_file_version(self, file_entry_id):
        self.REQ.endpoint = '/dlfileversion/get-latest-file-version'
        self.REQ.data = [
            ('fileEntryId', file_entry_id)
        ]
        return self.REQ.postREQ()
    # END DOCUMENT LIBRARY

    def add_expando_value(self, class_name, table_name, column_name, class_pk, data):
        self.REQ.endpoint = '/expandovalue/add-value'
        self.REQ.data = [
            ('companyId', self.COMPANY_ID),
            ('className', class_name),
            ('tableName', table_name),
            ('columnName', column_name),
            ('classPK', class_pk),
            ('data', data)
        ]
        return self.REQ.postREQ()

    def set_mysql_conn(self, conn):
        self._MYSQL_CONN = conn

    def add_article_to_layout(
            self,layout_id,layout_plid,article_id,type_setting,hash_val,purge_articles,articles_not_to_purge=[]):
        print('not to purge', articles_not_to_purge)
        if not self.update_layout3(layout_id, '\n'.join(type_setting)):
            print('didnt update layout')
            return False

        if not self.insert_scrape_rows(
                layout_id, layout_plid, [(hash_val, article_id)], purge_articles,articles_not_to_purge):
            print('didn\'t scrape')
            return False
        return True

    def get_article_id_from_hash(self, hash_val):
        if not self._MYSQL_CONN:
            print('need mysql to complete this request')
            exit(1)
        db = self._MYSQL_CONN
        cur = db.cursor()
        query = """
            SELECT articleId
            FROM JournalContentSearch
            WHERE portletId='{}'
        """.format(hash_val)
        cur.execute(query)
        return cur.fetchone()



    def insert_scrape_rows(self, layout_id, plid, portlet_map, purge_jarticles=True, articles_not_to_purge=[]):
        if not self._MYSQL_CONN:
            print('need mysql to complete this request')
            exit(1)
        db = self._MYSQL_CONN
        cur = db.cursor()

        preferences = '''<portlet-preferences>
                    <preference>
                        <name>ddmTemplateKey</name><value></value>
                    </preference>
                    <preference>
                        <name>enableComments</name><value>false</value>
                    </preference>
                    <preference>
                        <name>enableRelatedAssets</name><value>true</value>
                    </preference>
                    <preference>
                        <name>enableViewCountIncrement</name><value>true</value>
                    </preference>
                    <preference>
                        <name>groupId</name><value>{}</value>
                    </preference>
                    <preference>
                        <name>showAvailableLocales</name><value>false</value>
                    </preference>
                    <preference>
                        <name>enableCommentRatings</name><value>false</value>
                    </preference>
                    <preference>
                        <name>articleId</name><value>{}</value>
                    </preference>
                    <preference>
                        <name>enableRatings</name><value>false</value>
                    </preference>
                    <preference>
                        <name>enablePrint</name><value>false</value>
                    </preference>
                    <preference>
                        <name>extensions</name><value>NULL_VALUE</value>
                    </preference>
                </portlet-preferences>
            '''

        if purge_jarticles:
            print('purging journal articles!')
            # This section is to purge articles, journal content search , and portlet prefs
            arts2purge = []

            print(articles_not_to_purge,'articles not to purge')
            ntp = ''
            if articles_not_to_purge:
                print(articles_not_to_purge)
                ntp = "AND portletId NOT IN ('{}')".format("','".join(articles_not_to_purge))
                print(ntp)
            purge_articles = """
                SELECT articleId FROM JournalContentSearch
                WHERE
                    groupId = {} and
                    companyId = {} and
                    layoutId = {} {}
                """.format(self.GROUP_ID, self.COMPANY_ID, layout_id, ntp)

            cur.execute(purge_articles)
            res = cur.fetchall()
            if res is not None:
                print res
                for row in res:
                    row[0]
                    # if article on another jcs don't delete
                    cur.execute('SELECT contentSearchId FROM JournalContentSearch WHERE articleId = {}'.format(row[0]))
                    rowres = cur.fetchall()
                    if rowres is not None:
                        if len(rowres) > 1:
                            print('dont delte')
                        else:
                            arts2purge.append(row[0])

                cur.execute("""DELETE FROM JournalContentSearch WHERE layoutId = {}""".format(layout_id))
                db.commit()

                cur.execute("""DELETE FROM JournalArticle WHERE articleId IN ('{}')""".format("','".join(arts2purge)))
                db.commit()

                cur.execute("""DELETE FROM PortletPreferences WHERE plid = {}""".format(plid))
                db.commit()
                # END purging

        # self.get_current_count(db)

        counter = 0

        Qs = [
            ''' SELECT portletPreferencesId FROM PortletPreferences ORDER BY portletPreferencesId DESC LIMIT 1 ''',
            ''' SELECT currentId FROM Counter WHERE name = "com.liferay.counter.model.Counter" ''',
            ''' SELECT contentSearchId FROM JournalContentSearch ORDER BY contentSearchId DESC LIMIT 1 ''',
            ''' SELECT plid FROM Layout ORDER BY plid DESC LIMIT 1 ''',
            ''' SELECT id_ FROM JournalArticle ORDER BY id_ DESC LIMIT 1 '''
        ]
        for q in Qs:
            cur.execute(q)
            newIndex = cur.fetchone()
            if newIndex is not None:
                if newIndex[0] > counter: counter = newIndex[0]
        # db.close()

        if counter:
            self.DB_COUNT = counter

        for portlet in portlet_map:
            if len(portlet) == 2:

                q = '''
                    INSERT INTO JournalContentSearch
                    (contentSearchId,groupId,companyId,privateLayout,layoutId,portletId,articleId)
                    VALUES
                    ({},{},{},0,{},'{}',{})
                    '''.format(
                    self.DB_COUNT, self.GROUP_ID, self.COMPANY_ID, layout_id, portlet[0], portlet[1]
                )
                cur.execute(q)
                db.commit()
                self.DB_COUNT += 1

            q2 = '''
                INSERT INTO PortletPreferences
                (portletPreferencesId,ownerId,ownerType,plid,portletId,preferences)
                VALUES
                ({},0,3,{},"{}","{}")
                '''.format(
                self.DB_COUNT, plid, portlet[0], preferences.format(self.GROUP_ID, portlet[1])
            )

            cur.execute(q2)
            db.commit()
            self.DB_COUNT += 1

        update_counter_query = "UPDATE Counter SET currentId = {} WHERE name = 'com.liferay.counter.model.Counter'"
        cur.execute(update_counter_query.format(self.DB_COUNT))
        db.commit()

        db.close()

        # use try except and also rollback if needed
        return True

    @staticmethod
    def build_dl_url(dl_obj):
        return "/documents/{}/{}/{}/{}".format(
            dl_obj['groupId'],
            dl_obj['folderId'],
            dl_obj['title'],
            dl_obj['uuid']
        )
    @staticmethod
    def retXMLValue(xml_str, index):
        from xml.etree import cElementTree as ET
        xml_str = LiferayAPI.remove_bad_chars(xml_str)
        tree = ET.fromstring(xml_str)
        for node in tree.iter('root'):
            return node.findtext(index)

    @staticmethod
    def remove_bad_chars(s):
        print(type(s), 'this is the type')
        # print(type(s))
        # s = str(s)
        try:
            decoded_str = s.decode('utf-8')
            s = decoded_str
        except Exception as e:
            print e

        return s.replace(u"\u2018", "'").replace(u"\u2019", "'").replace(u"\u200b", "").replace(u"\u201c", '"').replace(
            u"\u201d", '"').replace(u'\u2013', '-').replace(u'\u2122', '&trade;').replace(u'\u03c0', '&pi;').replace(
            u'\xf3', '&oacute;').replace(u'\xe9', '&eacute;').replace(u'\xbd', '&frac12;').replace(u'\xed',
                                                                                                   '&iacute;').replace(
            u'\xe1', '&aacute;').replace(u'\xa1', '&iexcl;').replace(u'\u2026', '&hellip;').replace(u'\xae',
                                                                                                    '&reg;').replace(
            u'\u2022', '&bull;').replace(u'\u2014', '&mdash;').replace(u'\u2010', '-').replace(u'\xbb',
                                                                                               '&raquo;').replace(
            u'\xad', '&shy;').replace('0xc3', '&Agrave;').replace(u'\xa0','').replace('0xe2','&acirc;').replace(u'\xf1','&ntilde;')

    @staticmethod
    def append_hash_to_type_settings_column(ts, column):
        pass

    def genXMLContent(self,content_list, BASIC_CONTENT=False):
        root = ET.Element("root")
        root.set('avalible-locales', 'en_US')
        root.set('default-locale', 'en_US')

        if BASIC_CONTENT:
            self._genSubStaticElement(root, self.remove_bad_chars(content_list))

        else:

            for ele in content_list:
                self._genSubElement(root, ele)

        bareStr = ET.tostring(root, encoding='utf8', method='xml')
        return HTMLParser().unescape(bareStr)

    def _genSubElement(self, Parent, Ele, Index=0):

        CDataBLK = "<![CDATA[{}]]>"
        Index = str(Index)

        dynEle = ET.SubElement(Parent, 'dynamic-element', **Ele[0])
        dynEle.set('index-type', 'keyword')
        dynEle.set('index', Index)

        # if ele has 3 values loop through it and create sub elements of with content inside
        if len(Ele) >= 3:
            for cEle in Ele[2]:

                childDynEle = ET.SubElement(dynEle, 'dynamic-element', **cEle[0])
                childDynEle.set('index-type', 'keyword')
                childDynEle.set('index', Index)

                childDynCont = ET.SubElement(childDynEle, 'dynamic-content')
                childDynCont.set('language-id', 'en_US')
                childDynCont.text = CDataBLK.format(cEle[1])

                if len(cEle) >= 3:
                    for gcEle in cEle[2]:
                        gchildDynEle = ET.SubElement(childDynEle, 'dynamic-element', **gcEle[0])
                        gchildDynEle.set('index-type', 'keyword')
                        gchildDynEle.set('index', Index)

                        gchildDynCont = ET.SubElement(gchildDynEle, 'dynamic-content')
                        gchildDynCont.set('language-id', 'en_US')
                        gchildDynCont.text = CDataBLK.format(gcEle[1])

                Index = str(1 + int(Index))

        dynCont = ET.SubElement(dynEle, 'dynamic-content')
        dynCont.set('language-id', 'en_US')

        dynCont.text = CDataBLK.format(Ele[1])

    def _genSubStaticElement(self, Parent, ele):
        statEle = ET.SubElement(Parent, 'static-content')
        statEle.set('language-id', 'en_US')
        statEle.text = "<![CDATA[{}]]>".format(ele)

    class TypeSettings(object):
        def __init__(self, type_setting=''):
            self.layout_template_id = ''
            self.columns = {}
            self.type_setting_obj = {}
            self.additional_rows = []
            self.TS = []
            self.type_setting = ('' if not type_setting else self.parse_type_settings(type_setting))

        def parse_type_settings(self, ts):

            if isinstance(ts,unicode):
                ts = ts.encode('utf-8')
            rows = (ts.split('\n') if type(ts) == str else ts)

            for row in rows:
                if row.startswith('layout-template-id='):
                    self.layout_template_id = row[len('layout-template-id='):]
                elif row.startswith('column'):
                    column= row[7]
                    contents= row[9:]
                    if column not in self.columns:
                        self._add_column(column)
                    if row[8] == "=": #column hash row
                        self.columns[column]['hashes'] = contents.split(',')
                    elif row[8] == "-":
                        self.columns[column]['settings'].append(contents)
                else:
                    self.additional_rows.append(row)
            print(self.layout_template_id)

        def render_type_setting(self):
            for i,col in self.columns.iteritems():
                self.TS.append('column-{}={}'.format(i, ",".join(col['hashes'])))
                settings = set(col['settings'])
                for setting in settings:
                    self.TS.append('column-{}-{}'.format(i, setting))
            self.TS.append('layout-template-id={}'.format(self.layout_template_id))
            self.TS += self.additional_rows
            return "\n".join(self.TS)

        def _add_column(self,column):
            self.columns[column] = {
                'hashes': [],
                'settings': ['customizable=false'],
            }

        def append_hash_to_type_settings_column(self,hash_val,column,clear_hashes=False):
            if column not in self.columns:
                # check int in future or if cols
                self._add_column(column)
            if clear_hashes:
                self.columns[column]['hashes'] = []
            self.columns[column]['hashes'].append(hash_val)
            self.render_type_setting()
            return True

        def get_column_hashes(self, column):
            if column not in self.columns:
                return False
            return [h for h in self.columns[column]['hashes'] if h]


        @staticmethod
        def gen_type_setting_hash(portlet_id=56, rand_hash=''):
            ranger = random.randint(12, 16)
            if not rand_hash:
                rand_hash = ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ') for i in range(ranger))
            return "{}_INSTANCE_{}".format(portlet_id, rand_hash)
