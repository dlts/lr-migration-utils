# -*- coding: utf-8 -*-

import requests
import json


class REQ():
    def __init__(self, opts={}):

        self.host = (opts['host'] if 'host' in opts else '')
        self.headers = (opts['headers'] if 'headers' in opts else {})
        self.params = (opts['params'] if 'params' in opts else {})
        self.auth = (opts['auth'] if 'auth' in opts else ())
        self.data = (opts['data'] if 'data' in opts else [])

        self.endpoint = ''

        self.response = {}
        self.response_code = ''
        self.data = {}
        self.OBJType = ''  # set to address differences in data retreival
        self.requestOK = False

        self.exception = {
            'code': '',
            'message': ''
        }

    def _reqSetup(self, endpoint='', opts={}):
        self.endpoint = (endpoint if endpoint else "%s%s" % (self.host, self.endpoint))

    def renderData(self, OBJ):
        self.data = [(k, getattr(OBJ, k)) for k in vars(OBJ)]

    def getREQ(self, endpoint=''):
        self._reqSetup(endpoint)
        self._resetREQ()

        return self.requestExtract(requests.get(
            self.endpoint,
            headers=self.headers,
            data=self.data,
            params=self.params,
            auth=self.auth,
            verify=False
        ))

    def postREQ(self, endpoint="", multipart=False):
        self._reqSetup(endpoint)
        self._resetREQ()

        files = {}
        if multipart:
            for item in self.data:
                if item[0] == 'file':
                    files = {'file': item[1]}
                    self.data.remove(item)

        return self.requestExtract(requests.post(
            self.endpoint,
            headers=self.headers,
            data=self.data,
            params=self.params,
            auth=self.auth,
            files=files,
            verify=False
        ))

    def requestExtract(self, responseOBJ):
        self.response = responseOBJ
        self.response_code = self.response.status_code
        # print self.response
        # print self.response.text

        ## Target API response obj
        if self.OBJType == 'GC':
            try:
                # not always json response
                self.data = json.loads(self.response.text)['data']
            except ValueError as e:
                self.data = 'Request Successful'
            except KeyError as e:
                print(e)
        elif self.OBJType == 'LR':
            self.data = json.loads(self.response.text)
        else:
            self.data = self.response.text

        # should be verifying whether or not a json string and
        # if 'exception' not in self.data:
        self.requestOK = True
        return True

        self._setException(self.data)
        self._printException()
        return False

    def _setException(self, obj):
        self.exception['code'] = obj['exception']
        self.exception['message'] = obj['message']

    def _printException(self):
        print
        "Excpetion !!!"
        print
        "code:\t\t%s" % (self.exception['code'])
        print
        "message:\t%s" % (self.exception['message'])

    def _resetREQ(self):
        # self.data = []
        go = 1