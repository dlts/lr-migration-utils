from datetime import datetime
from json import dumps


class Utils():

    default_locale = 'en_US'

    @staticmethod
    def javaMap(val):
        return dumps({Utils.default_locale:val})


class Article():
    def __init__(self,D,groupId = 0):

        dt = datetime.now()

        self.groupId  =         (groupId if groupId else 0)
        self.folderId=          (D['folderId'] if 'folderId' in D else 0)
        self.classNameId=       0
        self.classPK=           0
        self.articleId=         '0'
        self.autoArticleId=     True
        self.titleMap=          Utils.javaMap(D['titleMap'])
        self.descriptionMap=    (Utils.javaMap(D['descriptionMap']) if 'descriptionMap' in D else '')
        self.content=           D['content']
        self.type=              'general'
        self.ddmStructureKey=   (D['ddmStructureKey'] if 'ddmStructureKey' in D else '')
        self.ddmTemplateKey=    (D['ddmTemplateKey'] if 'ddmTemplateKey' in D else '')
        self.layoutUuid=        ''
        self.displayDateMonth=  dt.month-1
        self.displayDateDay=    dt.day
        self.displayDateYear=   dt.year
        self.displayDateHour=   dt.hour -7
        self.displayDateMinute= dt.minute
        self.expirationDateMonth=   0
        self.expirationDateDay=     0
        self.expirationDateYear=    0
        self.expirationDateHour=    0
        self.expirationDateMinute=  0
        self.neverExpire =     True
        self.reviewDateMonth=   0
        self.reviewDateDay=     0
        self.reviewDateYear=    0
        self.reviewDateHour=    0
        self.reviewDateMinute=  0
        self.neverReview=       True
        self.indexable=         True
        self.articleURL=        ''


class Layout():
    def __init__(self,D,groupId = 0,layoutId = 0):
        self.groupId =          groupId
        self.privateLayout =    False
        self.layoutId =         D['layoutId']
        self.parentLayoutId =   0
        self.localeNamesMap =   Utils.javaMap(D['name'])
        self.localeTitlesMap =  Utils.javaMap(D['name'])
        self.descriptionMap =   Utils.javaMap(D['description'])
        self.robotsMap =        Utils.javaMap(D['robotsMap'])
        self.type =             'general'
        self.hidden =           False
        self.friendlyURL =      (D['friendlyURL'] if 'friendlyURL' in D  else '')
        self.iconImage =        False
        self.iconBytes =        ''


class DLFile():
    def __init__(self,D,groupId):
        self.repositoryId = groupId
        self.folderId = (D['folderId'] if 'folderId' in D else 0)
        self.sourceFileName = D['sourceFileName']
        self.mimeType = D['mimeType']
        self.title = D['title']
        self.description = D['description']
        self.changeLog = "1"
        self.file = D['file']
