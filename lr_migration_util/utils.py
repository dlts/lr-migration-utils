# -*- coding: utf-8 -*-

import datetime,time
from urlparse import urlparse,urljoin

from bs4 import BeautifulSoup


def get_images_from_html(html, url, selector):
    parsed_uri = urlparse(url)
    domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)

    soup = BeautifulSoup(html, 'html.parser')
    img_eles = soup.select(selector)

    data = []
    for img_src in img_eles:
        if img_src.get('src')[0] == '/':
            data.append((urljoin(domain, img_src.get('src')), img_src.get('src')))
    return data


def get_content_from_html(html):
    soup = BeautifulSoup(html, 'html.parser')
    tcp = soup.find('div', class_='twelve')
    for div in tcp.find_all("div", {'class': 'subpage-h-container'}):
        div.decompose()
    # body = soup.find('body')
    # tcp = soup.find('div', class_='container', recursive=False)
    # print tcp.contents

    # for row in tcp.contents:
    #     print row
    #     str(row.encode('utf-8'))
    # exit(0)
    return ''.join([str(x.encode('utf-8')) for x in tcp.contents])


def log_pages(result, source, target, desired_layout):
    right_now = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    data = [str(result), source, target, desired_layout ,right_now]
    row = '"' + '","'.join(data) + '"'
    with open("pages.log", "a+") as f:
        f.write("{}\n".format(row))
    return