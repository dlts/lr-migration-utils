import MySQLdb


class DB:
    def __init__(self, host, user, password, database, port='3306'):
        self.conn = self.conn(host, port, user, password, database)

    def conn(self, host, port, user, password, database):
        return MySQLdb.connect(
            host=host,
            port=port,
            user=user,
            passwd=password,
            db=database
        )